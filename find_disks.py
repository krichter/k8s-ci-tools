#!/usr/bin/python3

import subprocess as sp
import json
import logging


def find_disks(output_file="disk-name-suffixes", kubectl_binary="kubectl"):
    kubectl_output = sp.check_output([kubectl_binary, "get", "pvc", "--all-namespaces", "--output=json"])
    kubectl_output_load = json.loads(kubectl_output.decode("utf-8"))
    with open(output_file, "w") as file:
        for obj in kubectl_output_load['items']:
            if obj['kind'] == "PersistentVolumeClaim":
                disk_name_suffix = obj['spec']['volumeName']
                logging.info("found pvc '%s' which is added as disk name suffix" % (disk_name_suffix,))
                file.write("%s\n" % (disk_name_suffix,))


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    find_disks()
