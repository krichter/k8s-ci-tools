#!/usr/bin/python3

import subprocess as sp
import time
import logging
import plac


@plac.annotations(service_name=plac.Annotation("The name of the service whose IP to poll", kind="option"),
                  installation_namespace=plac.Annotation("The name of the namespace ingress has been installed in", kind="option"),
                  )
def wait_for_ingress_ip(service_name="ingress-nginx-controller", installation_namespace="ingress-nginx", kubectl="kubectl"):
    wait_max_secs = 600
    wait_interval = 5
    count = 0
    ip = ""
    while count < wait_max_secs:
        ip = sp.check_output([kubectl, "get", "service", "--namespace=%s" % (installation_namespace,), "--output=jsonpath='{.items[?(@.metadata.name==\"%s\")].status.loadBalancer.ingress[0].ip}'" % (service_name,)]).decode("utf-8").strip()
        logging.debug("ip: %s" % (ip,))
        if ip != "" and ip != "''":
            break
        count += wait_interval
        time.sleep(wait_interval)
    if ip == "" or ip == "''":
        raise ValueError("ip could not be retrieved within %d seconds" % (wait_max_secs,))
    print(ip)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    plac.call(wait_for_ingress_ip)
