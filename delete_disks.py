#!/usr/bin/python3

import subprocess as sp
import logging


def delete_disks(input_file="disk-name-suffixes", gcloud="gcloud"):
    disks_output = sp.check_output([gcloud, "compute", "disks", "list"]).decode("utf-8").strip()
    if len(disks_output) == 0:
        return
    disks_output_lines = disks_output.split("\n")
    disk_names = list(filter(lambda disk_name_line: disk_name_line != "NAME", list(map(lambda line: line.split()[0], disks_output_lines))))
    logging.debug("disk_names: %s" % (str(disk_names),))

    with open(input_file) as file:
        disk_name_suffixes = [line.strip() for line in file.readlines()]
        for disk_name_suffix in disk_name_suffixes:
            matching_disk_name = matches_disk_name(disk_name_suffix, disk_names)
            if matching_disk_name is None:
                logging.debug("ignoring disk name suffix '%s' because it doesn't match any disk names" % (disk_name_suffix,))
            else:
                logging.info("deleting disk '%s'" % (matching_disk_name,))
                sp.check_call([gcloud, "compute", "disks", "delete", "--quiet", matching_disk_name])


def matches_disk_name(disk_name_suffix, disk_names):
    for disk_name in disk_names:
        if disk_name.endswith(disk_name_suffix):
            return disk_name
    return None


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    delete_disks()
